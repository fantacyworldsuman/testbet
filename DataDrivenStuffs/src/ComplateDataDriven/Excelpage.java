package ComplateDataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excelpage {

	XSSFSheet sheet;
	XSSFWorkbook wb;
	
	public Excelpage(String path) throws Exception
	{
		File src = new File(path);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);		
	}
	
	public int rowCount(int sheetIndex)
	{
		int row = wb.getSheetAt(sheetIndex).getLastRowNum();
		row = row + 1;
		return row;
	}
	
	public String getData(int sheetNum, int rowNum, int colNum)
	{
		sheet = wb.getSheetAt(sheetNum);
		String value = sheet.getRow(rowNum).getCell(colNum).getStringCellValue();
		return value;
	}
}
