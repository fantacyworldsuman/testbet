package ComplateDataDriven;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginPage {

	WebDriver dr;
	
	@Test(dataProvider="cast")
	public void login(String un, String pw)
	{
		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		dr = new ChromeDriver();
		dr.get("https://pi.cashpoint.com");
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		dr.findElement(By.id("username")).sendKeys(un);
		dr.findElement(By.id("password")).sendKeys(pw);
		dr.findElement(By.id("submit")).click();

	}
	
	@DataProvider(name="cast")
	public Object[][] passData() throws Exception
	{
		Excelpage config = new Excelpage("E:\\workspace\\DataDrivenStuffs\\TestExcelFolder\\TestExcelData.xlsx");
		int rowNum = config.rowCount(0);
		
		Object[][] data = new Object[rowNum][2];
		
		for(int i=0; i<rowNum; i++)
		{
			data[i][0] = config.getData(0, i, 0);
			data[i][1] = config.getData(0, i, 1);
		}
		return data;
	}
}
