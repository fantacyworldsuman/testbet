package ReadExcelData;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {

	public static void main(String[] args) throws Exception
	{
		File src = new File("E:\\Suman\\Softwares\\Cell\\Excel\\New_One\\TestData.xlsx");
		FileInputStream fis = new FileInputStream(src);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet = wb.getSheetAt(0);
		
		//System.out.println(sheet.getRow(0).getCell(0).getStringCellValue());
		int rowcount = sheet.getLastRowNum();
		System.out.println("Total number of row is : " + rowcount + "\n");
		for(int i=0;i<rowcount;i++)
		{
			String cellValue = sheet.getRow(i).getCell(1).getStringCellValue();
			System.out.println("The appeared value is : " + cellValue);
		}
		
		
	}
}
