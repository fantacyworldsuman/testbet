package DataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class Read {
	
	@Test
	public void dataRead() throws Exception
	{
		File file = new File("E:\\Suman\\Softwares\\Cell\\ExcelNew\\TestOne.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sheet = wb.getSheetAt(0);
		
		int row = sheet.getLastRowNum();
		row = row + 1;
		for(int i=0; i<row; i++)
		{
			System.out.println("Row is : " + sheet.getRow(i).getCell(0).getStringCellValue());
			System.out.println("Column is : " + sheet.getRow(i).getCell(1).getStringCellValue());
		}
		
		FileOutputStream fos = new FileOutputStream(file);
		//sheet.createRow(4).createCell(5).setCellValue("gos");
		sheet.createRow(4).createCell(5).setCellValue(" ");
		sheet.getRow(2).createCell(2).setCellValue("hello");
		wb.write(fos);
		
	}

}