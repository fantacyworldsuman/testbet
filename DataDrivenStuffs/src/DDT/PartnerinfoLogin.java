package DDT;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PartnerinfoLogin {

	WebDriver dr;
	
	@Test(dataProvider="fd")
	public void piLogin(String usnm, String pswd) throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\DataDrivenStuffs\\lib\\Driver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.get("https://pi.cashpoint.com");
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		dr.findElement(By.id("username")).sendKeys(usnm);
		dr.findElement(By.id("password")).sendKeys(pswd);
		dr.findElement(By.id("submit")).click();
		Thread.sleep(4000);
		Assert.assertTrue(dr.getTitle().contains("Partnerinfo"), "Not redirected to proper page");
		System.out.println("User navigated to proper page.");
	}

	//@AfterMethod
	public void tear()
	{
		dr.close();
	}
	
	@DataProvider(name="fd")
	public Object[][] dataPass()
	{
		Object[][] data = new Object[3][2];
		
		data[0][0] = "pitest1";
		data[0][1] = "$Pitest#1";
		
		data[1][0] = "pitest2";
		data[1][1] = "$Pitest#2";
		
		data[2][0] = "pitest1";
		data[2][1] = "$Pites";
		
		return data;
	}
	
	
}
