package lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataConfig {

	XSSFWorkbook wkbk;
	XSSFSheet sht;
	
	public ExcelDataConfig(String excelPath) throws Exception
	{
		try {
			File file = new File(excelPath);
			FileInputStream fis = new FileInputStream(file);
			wkbk = new XSSFWorkbook(fis);
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		} 		
	}
	public String getData(int sheetNumber, int rowNumber, int columnNumber)
	{
		sht = wkbk.getSheetAt(sheetNumber);
		String data = sht.getRow(rowNumber).getCell(columnNumber).getStringCellValue();
		return data;

	}
	
}
